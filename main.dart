import 'solutions/solution1.dart';
import 'solutions/solution2.dart';
import 'solutions/solution3.dart';
import 'solutions/solution4.dart';
import 'solutions/solution5.dart';

main() {
  //Problem 1
  final list1 = [7, 19, 5, 9, 8];
  sortAscendingWithSingleLoop(list1);
  print('Result 1 --> $list1');

  //Problem 2
  final list2 = [1, 4, 17, 7, 25, 3, 100];
  final result = getKLargestElements(list2, 3);
  print('Result 2 --> $result');

  //Problem 3
  final list3 = [4, 7, 2, 3, 2, 7, 4];
  final numberOfDistinctValues = getNumberOfDistinctValues(list3);
  print('Result 3 --> $numberOfDistinctValues');

  //Problem 4
  final escapeDays = calculateEscapeDays(20);
  print('Result 4 --> $escapeDays');

  //Problem 5
  final bigString = "this is a big string";
  final smallStrings = ["this", "you", "is", "a", "bigger", "string", "noted"];
  final result5 = doesContainSmallStrings(
    bigString: bigString,
    smallStrings: smallStrings,
  );
  print('Result 5 --> $result5');
}

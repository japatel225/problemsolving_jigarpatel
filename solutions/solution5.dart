List<bool> doesContainSmallStrings({
  required String bigString,
  required List<String> smallStrings,
}) {
  return smallStrings
      .map((smallString) => bigString.contains(smallString))
      .toList();
}

int calculateEscapeDays(int wellDepth) {
  if (wellDepth <= 3) return 1; //will escape in 1 leap

  //leaps 3 metre, falls back 2 metre everyday
  //so 1 metre everyday for (wellDepth - 3), plus one last leap for remaining 3 metres
  final leaps = (wellDepth - 3) + 1;

  return leaps; //leaps = days, because one leap is taken everyday
}

void sortAscendingWithSingleLoop(List<int> list) {
  for (int i = 0; i < list.length - 1; i++) {
    if (list[i] > list[i + 1]) {
      swapElementsAtXAndY(list, x: i, y: i + 1);
      i = -1;
    }
  }
}

void swapElementsAtXAndY(List<int> list, {required int x, required int y}) {
  final temp = list[x];
  list[x] = list[y];
  list[y] = temp;
}

List<int> getKLargestElements(List<int> list, int k) {
  final result = list.sublist(0, k);

  int indexOfSmallestInResult = getIndexOfSmallest(result);
  for (int i = k; i < list.length; i++) {
    if (list[i] > result[indexOfSmallestInResult]) {
      result[indexOfSmallestInResult] = list[i];
      indexOfSmallestInResult = getIndexOfSmallest(result);
    }
  }

  return result;
}

int getIndexOfSmallest(List<int> list) {
  int indexOfSmallest = 0;

  for (int i = 1; i < list.length; i++) {
    if (list[i] < list[indexOfSmallest]) indexOfSmallest = i;
  }

  return indexOfSmallest;
}

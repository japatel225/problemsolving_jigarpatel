int getNumberOfDistinctValues(List<int> list) => list.toSet().length;
